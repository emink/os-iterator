const backend_url = "http://localhost:8080/api";
const endpoint_links = $('nav .container-fluid ul li ul');

const computeTableSettings = {
    lengthMenu: [[15, 20, 25, 50], [15, 20, 25, 50]],
    // scrollX: true,
    columns: [
        {data: 'id'},
        {data: 'tenant_id'},
        {data: 'name'},
        {data: 'status'},
        {
            data: 'flavor',
            render: function (data) {
                return formatFlavorObj(data);
            }
        },
        {
            data: 'addresses',
            render: function(data) {
                return formatAddressesObj(data);
            }
        }
    ]
}

const baremetalTableSettings = {
    lengthMenu: [[15, 20, 25, 50], [15, 20, 25, 50]],
    scrollX: true,
    columns: [
        {data: 'uuid'},
        {data: 'name'},
        {data: 'instance_uuid'},
        {data: 'provision_state'},
        {data: 'resource_class'},
        {data: 'maintenance'},
        {data: 'maintenance_reason'},
        {data: 'power_state'},
    ]
}


const imageTableSettings = {
    lengthMenu: [[15, 20, 25, 50], [15, 20, 25, 50]],
    scrollX: true,
    columns: [
        {data: 'id'},
        {data: 'name'},
        {data: 'status'},
        {data: 'visibility'},
        {data: 'container_format'},
        {data: 'disk_format'},
        {data: 'virtual_size'},
        {data: 'file'},
        {data: 'checksum'},
    ]
}

const networkTableSettings = {
    lengthMenu: [[15, 20, 25, 50], [15, 20, 25, 50]],
    // scrollX: true,
    columns: [
        {data: 'id'},
        {data: 'name'},
        {data: 'status'},
        {data: 'subnets'},
        {data: 'project_id'},
        {data: 'shared'},
    ]
}

const volumeTableSettings = {
    lengthMenu: [[15, 20, 25, 50], [15, 20, 25, 50]],
    // scrollX: true,
    columns: [
        {data: 'id'},
        {data: 'name'},
        {data: 'status'},
        {data: 'size'},
        {data: 'volume_type'},
        // {data: 'attachments'},
        {data: 'user_id'},
    ]
}

const portTableSettings = {
    lengthMenu: [[15, 20, 25, 50], [15, 20, 25, 50]],
    scrollX: true,
    columns: [
        {data: 'id'},
        {data: 'name'},
        {data: 'status'},
        {data: 'network_id'},
        {data: 'mac_address'},
        {data: 'fixed_ips'},
        {data: 'project_id'},
        {data: 'device_id'},
    ]
}

const loadbalancerTableSettings = {
    lengthMenu: [[15, 20, 25, 50], [15, 20, 25, 50]],
    scrollX: true,
    columns: [
        {data: 'id'},
        {data: 'name'},
        {data: 'provisioning_status'},
        {data: 'operating_status'},
        {data: 'project_id'},
        {data: 'vip_address'},
        {data: 'vip_port_id'},
        {data: 'vip_network_id'},
        {data: 'vip_subnet_id'},
        {data: 'flavor_id'},
    ]
}

let active = "compute";

function toggleDesc() {
    $('div.toggler').map(function (_, value) {
        $(value).next('div').toggle();
    });
}

function formatFlavorObj(data) {
    const name = data["original_name"];
    const cpu = data["vcpus"];
    const ram = data["ram"];
    return '<div class="btn btn-outline-primary btn-sm toggler" onclick="toggleDesc()">'+name+'</div>'+
        '<div class="hidden"><i class="bi bi-cpu"> '+cpu+'</i>'+
        ' <i class="bi bi-memory"> '+ram+'</i></div>';
}

function formatAddressesObj(data) {
    let result = '';
    $.each(data, function(name, networks) {
        result += '<div class="btn btn-outline-primary btn-sm toggler" onclick="toggleDesc()">'+name+'</div>';
        networks.forEach(function(network) {
            result = result+
                '<div class="hidden">'+
                'version='+network["version"]+'</br>'+
                'ip='+network["addr"]+'</br>'+
                'mac='+network["OS-EXT-IPS-MAC:mac_addr"]+'</div> ';
        });
    });
    return result;
}

function initEndpointMenu(endpoints) {
    endpoint_links.children().remove();
    $.each(endpoints, function(index, endpoint) {
        endpoint_links.append("<li><a class=\"dropdown-item\" href=\"#\">"+endpoint+"</a></li>");
    })
    $(endpoint_links).on('click', 'a', function () {
        // e.stopPropagation();
        // e.stopImmediatePropagation();
        // e.preventDefault();
        $(endpoint_links).find('a').removeClass('active');
        $(this).addClass("active");
        const before = active;
        active = $(this).text();
        console.log(`before=${before}active=${active}`);
        load(active);
    });
}

async function newTable(endpoint) {
    const container = $('#tables');
    return await $.when($.get(`./tables/${endpoint}.html`)).then(function(content) {
        container.children().remove();
        container.append(content);
        // console.log("endpoint=" + endpoint+", content="+content);
        switch (endpoint) {
            case "compute":
                return new DataTable('#' + endpoint, computeTableSettings);
            case "baremetal":
                return new DataTable('#' + endpoint, baremetalTableSettings);
            case "image":
                return new DataTable('#' + endpoint, imageTableSettings);
            case "network":
                return new DataTable('#' + endpoint, networkTableSettings);
            case "volume":
                return new DataTable('#' + endpoint, volumeTableSettings);
            case "port":
                return new DataTable('#' + endpoint, portTableSettings);
            case "loadbalancer":
                return new DataTable('#' + endpoint, loadbalancerTableSettings);
        }
    });
}

function fetchEndpoint(site, endpoint, def) {
    $.getJSON(backend_url + "/" + site + "/" + endpoint, function (data) {
        if (data["resources"] !== null && data["resources"].length > 0) {
            def.then(function (table) {
                if (table === undefined) {
                    return;
                }
                table.rows.add(data["resources"]).draw();
                console.log(`New rows: site=${data["site"]}, endpoint=${data["type"]}, count=${data["resources"].length}`);
            })
        }
    }).fail(function () {
        console.log("failed to get site=" + site + ", endpoint=" + endpoint);
    });
}

function load(endpoint) {
    let def = newTable(endpoint);
    $.getJSON(backend_url + "/" + "stats", function(data) {
        initEndpointMenu(data["endpoints"]);

        for (let i = 0; i < data["sites"].length; i++) {
            for (let j = 0; j < data["endpoints"].length; j++) {
                if (data["endpoints"][j] === endpoint) {
                    fetchEndpoint(data["sites"][i], endpoint, def);
                    break;
                }
            }
        }
    });
}

$(document).ready(function() {
    load(active);
});

