package main

import (
	"gitlab.com/emink/os-iterator/pkg/os-iterator/cmd"
)

func main() {
	cmd.Execute()
}
