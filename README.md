# Table of Contents

* [Iterator](#iterator)
    * [Build](#build)
    * [Parameters](#parameters)
    * [Run](#run)
    * [Routes](#routes)
    * [Web UI](#web-ui)
    * [TODO](#todo)

# Iterator

![UI Example](/frontend/images/ui_example.png)

The iterator consists of two parts:

1. The Go backend is responsible for collecting data from the open stack regions described in the configuration file.
   And also for storing data and processing rest requests to retrieve data
2. The JS frontend manages the initialization and processing of tables with data from openstack endpoints,
   the resources in the table are presented in a multi-regional form for quick search.
   To minimize code writing and ease of table processing, the datatables library was chosen,
   bootstrap was chosen for visualisation and jQuery for asynchronous requests to backend

## Build

```shell
go build -o os-iterator ./cmd/os-iterator/main.go
```

## Parameters

Project use cobra for flags management
```shell
$ ./os-iterator --help                     
Asynchronous operations across clouds

Usage:
  os-iterator [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  serve       Start restful API

Flags:
  -a, --attempts int        limit the maximum number of attempts to recover the last error (default 10)
  -c, --config string       Config file name (default "clouds.yaml")
  -d, --debug               Enable debug logs
  -e, --endpoints strings   endpoints to iterate through (default [compute,baremetal,network,port,volume,image,loadbalancer])
  -h, --help                help for os-iterator
  -i, --interval int        interval between iterations in minutes (default 30)
  -p, --parallelism int     parallelism limits the number of goroutines that can run simultaneously (default 8)
  -s, --sites strings       sites to iterate through (default is generated from clouds config)

Use "os-iterator [command] --help" for more information about a command.
```
> The project is at the development stage, not all parameters and options can be set by flags
> There is no authentication module, so access is hardcoded to localhost:8080

## Run

```shell

./os-iterator -a 5 -s RegionOne,RegionTwo -e compute,volume serve
```

The example above will:

1. Give 5 tries to do the job (timeouts, context deadline cancellation, ...)
2. Iterate every 30 minutes and saving data
3. Iterate only through RegionOne, RegionTwo
4. Fetch only servers and volumes
5. Write well formatted backend logs to iterator.log
6. Write gin statistic about rest requests to stdout
7. Try to save data in Redis DB listening on localhost:6379

> The storage currently only supports Radis DB.
> It used to load data during backend start.
> It will throw error log but continue to work even if data couldn't be saved

## Routes

```
[GIN-debug] GET    /ui/*filepath             --> github.com/gin-gonic/gin.(*RouterGroup).createStaticHandler.func1 (3 handlers)
[GIN-debug] HEAD   /ui/*filepath             --> github.com/gin-gonic/gin.(*RouterGroup).createStaticHandler.func1 (3 handlers)
[GIN-debug] GET    /api/stats                --> gitlab.com/emink/os-iterator/pkg/os-iterator/cmd.handleStats (3 handlers)
[GIN-debug] GET    /api/search               --> gitlab.com/emink/os-iterator/pkg/os-iterator/cmd.handleSearch (3 handlers)
[GIN-debug] GET    /api/:site                --> gitlab.com/emink/os-iterator/pkg/os-iterator/cmd.handleGetSite (3 handlers)
[GIN-debug] GET    /api/:site/:endpoint      --> gitlab.com/emink/os-iterator/pkg/os-iterator/cmd.handleGetEndpoint (3 handlers)
[GIN-debug] POST   /api/:site/:endpoint      --> gitlab.com/emink/os-iterator/pkg/os-iterator/cmd.handlePostEndpoint (3 handlers)
```

## Web UI

Open `http://localhost:8080/ui`

# TODO

List of todos will be presented soon...