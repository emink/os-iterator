package utils

import (
	"reflect"
	"regexp"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	t time.Time
)

func Sleep(start time.Time, interval int) {
	elapsed := time.Since(start)
	left := time.Duration(interval)*time.Minute - elapsed
	if left > 0 {
		log.WithFields(log.Fields{"interval": left}).Info("Sleep before next iteration")
		time.Sleep(left)
	}
}

func CompareResource(res interface{}, opts map[string]interface{}) bool {
	resValue := reflect.ValueOf(res)
	if resValue.Kind() == reflect.Ptr {
		resValue = resValue.Elem()
	}

	resType := reflect.TypeOf(res)
	if resType.Kind() == reflect.Ptr {
		resType = resType.Elem()
	}

	var matches int
	if resValue.Kind() == reflect.Struct {
		for i := 0; i < resValue.NumField(); i++ {
			v := resValue.Field(i)
			f := resType.Field(i)
			tag := f.Tag.Get("json")
			if tag != "" {
				if !isZero(v) {
					if opts[tag] != nil {
					loop:
						switch v.Kind() {
						case reflect.Ptr:
							v = v.Elem()
							goto loop
						case reflect.String:
							if tag == "id" || tag == "uuid" {
								if v.String() == opts[tag] {
									return true
								}
							} else if tag == "name" {
								if opts[tag].(*regexp.Regexp).MatchString(v.String()) {
									matches++
								}
							} else {
								if v.String() == opts[tag] {
									matches++
								}
							}
						case reflect.Bool:
							if v.Bool() == opts[tag] {
								matches++
							}
						default:
							return false
							//case reflect.Map:
							//for _, key := range v.MapKeys() {
							//	vType := v.MapIndex(key)
							//	if vType.Kind() == reflect.Interface {
							//		if mapField := vType.Elem(); mapField.Kind() == reflect.Slice {
							//			for idx := 0; idx < mapField.Len(); idx++ {
							//				trueValue := mapField.Index(idx).Interface()
							//				trueTrueValue := reflect.ValueOf(trueValue)
							//				log.WithFields(log.Fields{
							//					"map_kind":   mapField.Kind(),
							//					"field_type": mapField.Type(),
							//					"field":      mapField.Index(idx),
							//					"true_value": trueTrueValue.Kind(),
							//				}).Info("Map description")
							//			}
							//		}
							//	}
							//}
						}
					}
				}
			}
		}
	}
	//log.WithFields(log.Fields{
	//	"field":       tag,
	//	"type":        f.Type,
	//	"filter":      opts[tag],
	//	"filter_type": reflect.TypeOf(opts[tag]).Kind(),
	//}).Debug("Comparing resource")
	return matches == len(opts)
}

func isZero(v reflect.Value) bool {
	//fmt.Printf("\n\nchecking isZero for value: %+v\n", v)
	switch v.Kind() {
	case reflect.Ptr:
		if v.IsNil() {
			return true
		}
		return false
	case reflect.Func, reflect.Map, reflect.Slice:
		return v.IsNil()
	case reflect.Array:
		z := true
		for i := 0; i < v.Len(); i++ {
			z = z && isZero(v.Index(i))
		}
		return z
	case reflect.Struct:
		if v.Type() == reflect.TypeOf(t) {
			if v.Interface().(time.Time).IsZero() {
				return true
			}
			return false
		}
		z := true
		for i := 0; i < v.NumField(); i++ {
			z = z && isZero(v.Field(i))
		}
		return z
	}
	// Compare other types directly:
	z := reflect.Zero(v.Type())
	//fmt.Printf("zero type for value: %+v\n\n\n", z)
	return v.Interface() == z.Interface()
}
