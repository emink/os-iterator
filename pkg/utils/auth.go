package utils

import (
	log "github.com/sirupsen/logrus"
	"time"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/utils/openstack/clientconfig"
)

const (
	httpClientTimeout     = 60
	ErrNoSuitableEndpoint = "No suitable endpoint could be found in the service catalog."
)

func NewServiceClient(site string, epName string) (*gophercloud.ServiceClient, error) {
	opts := &clientconfig.ClientOpts{Cloud: site}
	sc, err := clientconfig.NewServiceClient(epName, opts)
	if err != nil {
		return nil, err
	}
	log.WithFields(log.Fields{"cloud": site, "endpoint": epName}).Info("Created new Service Client")
	switch epName {
	case "baremetal":
		sc.Microversion = "1.72"
	case "compute":
		sc.Microversion = "2.88"
	}
	sc.HTTPClient.Timeout = time.Second * httpClientTimeout
	return sc, err
}
