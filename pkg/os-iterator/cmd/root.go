package cmd

import (
	"fmt"
	"gitlab.com/emink/os-iterator/pkg/config"
	"os"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

const LogFile = "iterator.log"

var (
	cfg                = &config.Config{}
	supportedSites     []string
	supportedEndpoints = []string{
		"compute", "baremetal", "network",
		"port", "volume", "image", "loadbalancer",
	}
	rootCmd = &cobra.Command{
		Use:   "os-iterator",
		Short: "Asynchronous operations across clouds",
		Long:  `Asynchronous operations across clouds`,
	}
)

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd.Execute()
}

func init() {
	logFile, err := os.OpenFile(LogFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		fmt.Printf("Error while opening '%s' for write", "iterator.log")
		os.Exit(1)
	}
	log.SetOutput(logFile)
	log.SetFormatter(&log.TextFormatter{
		ForceColors:            false,
		FullTimestamp:          true,
		DisableLevelTruncation: true,
		PadLevelText:           true,
	})

	log.RegisterExitHandler(func() {
		logFile.Close()
	})
	rootCmd.PersistentFlags().BoolVarP(&cfg.Debug, "debug", "d", false, "Enable debug logs")
	rootCmd.PersistentFlags().StringVarP(&cfg.ConfigPath, "config", "c", "clouds.yaml", "Config file name")
	rootCmd.PersistentFlags().IntVarP(&cfg.Attempts, "attempts", "a", 10, "limit the maximum number of attempts to recover the last error")
	rootCmd.PersistentFlags().IntVarP(&cfg.Interval, "interval", "i", 30, "interval between iterations in minutes")
	rootCmd.PersistentFlags().IntVarP(&cfg.Parallelism, "parallelism", "p", runtime.NumCPU(), "parallelism limits the number of goroutines that can run simultaneously")
	rootCmd.PersistentFlags().StringSliceVarP(&cfg.Sites, "sites", "s", supportedSites, "sites to iterate through (default is generated from clouds config)")
	rootCmd.PersistentFlags().StringSliceVarP(&cfg.Endpoints, "endpoints", "e", supportedEndpoints, "endpoints to iterate through")
	//searchCmd.PersistentFlags().StringVarP(&filtersArg, "filters", "", "", "Filter resources by common fields value")
	//updateCmd.PersistentFlags().StringVarP(&filtersArg, "filters", "", "", "Update resources by common fields value")
	rootCmd.AddCommand(serveCmd)

	cobra.OnInitialize(initConfig, validateSites, validateEndpoints)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfg.Debug {
		log.SetLevel(log.DebugLevel)
	}
	log.WithFields(log.Fields{"threads": runtime.NumCPU(), "parallelism": cfg.Parallelism}).Debug("Async settings")
}

func getSupportedSites() []string {
	cloudsCfg, err := config.ReadCloudsYAML(cfg.ConfigPath)
	if err != nil {
		log.WithFields(log.Fields{"config": cfg.ConfigPath, "error": err}).Fatal("Unable to read clouds config")
	}
	log.WithFields(log.Fields{"file": cfg.ConfigPath}).Debug("Using config")
	var sites []string
	for site := range cloudsCfg.Clouds {
		sites = append(sites, site)
	}
	return sites
}

func validateSites() {
	supportedSites = getSupportedSites()
	if cfg.Sites != nil {
		in, out := difference(cfg.Sites, supportedSites)
		if len(out) > 0 || len(in) == 0 {
			log.WithFields(log.Fields{
				"supported":   strings.Join(in, ","),
				"unsupported": strings.Join(out, ","),
			}).Warn("Unsupported sites")
		}
		cfg.Sites = in
	} else {
		cfg.Sites = supportedSites
	}
	log.WithFields(log.Fields{"sites": strings.Join(cfg.Sites, ",")}).Debug("Target sites")
}

func validateEndpoints() {
	if cfg.Endpoints != nil {
		in, out := difference(cfg.Endpoints, supportedEndpoints)
		if len(out) > 0 || len(in) == 0 {
			log.WithFields(log.Fields{
				"supported":   strings.Join(in, ","),
				"unsupported": strings.Join(out, ","),
			}).Warn("Unsupported endpoints")
		}
		cfg.Endpoints = in
	} else {
		cfg.Endpoints = supportedEndpoints
	}
	log.WithFields(log.Fields{"endpoints": cfg.Endpoints}).Debug("Target endpoints")
}

func difference(a, b []string) (in, out []string) {
	mb := make(map[string]struct{}, len(b))
	for _, x := range b {
		mb[x] = struct{}{}
	}
	for _, x := range a {
		if _, ok := mb[x]; !ok {
			out = append(out, x)
		} else {
			in = append(in, x)
		}
	}
	return
}
