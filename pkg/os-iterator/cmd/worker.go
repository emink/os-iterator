package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"gitlab.com/emink/os-iterator/pkg/iterator"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

var (
	iter     iterator.Iterator
	serveCmd = &cobra.Command{
		Use:   "serve",
		Short: "Start restful API",
		Long:  `Start restful API`,
		Run:   serve,
	}
)

type RequestBody struct {
	Action string `json:"action"`
}

func serve(_ *cobra.Command, _ []string) {
	s := store.New(map[string]string{"type": "redis"})
	iter = iterator.New(cfg, s).Auth().Load()
	go iter.Fetch()
	r := gin.Default()
	//r.Use(CORSMiddleware())
	r.Static("/ui", "./frontend")
	r.GET("/api/stats", handleStats)
	r.GET("/api/search", handleSearch)
	r.GET("/api/:site", handleGetSite)
	r.GET("/api/:site/:endpoint", handleGetEndpoint)
	r.POST("/api/:site/:endpoint", handlePostEndpoint)
	//r.GET("/", func(c *gin.Context) {
	//
	//})
	r.Run("127.0.0.1:8080")
}

// For debug purposes only
//func CORSMiddleware() gin.HandlerFunc {
//	return func(c *gin.Context) {
//		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
//		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
//		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
//		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")
//
//		if c.Request.Method == "OPTIONS" {
//			c.AbortWithStatus(204)
//			return
//		}
//		c.Next()
//	}
//}

//func checkSite(site string) bool {
//	if in, _ := difference([]string{site}, cfg.Sites); len(in) == 0 {
//		return false
//	}
//	return true
//}
//
//func checkEndpoint(epName string) bool {
//	if in, _ := difference([]string{epName}, cfg.Endpoints); len(in) == 0 {
//		return false
//	}
//	return true
//}

func handleStats(c *gin.Context) {
	c.JSONP(http.StatusOK, gin.H{
		"sites":     cfg.Sites,
		"endpoints": cfg.Endpoints,
		"status":    "ok",
	})
}

func handleGetSite(c *gin.Context) {
	site := strings.ToUpper(c.Param("site"))
	eps, ok := iter.GetSite(site)
	if !ok {
		c.JSONP(http.StatusNotFound, gin.H{
			"site":    site,
			"message": "Endpoints not tracking by backend",
		})
		return
	}
	c.JSONP(http.StatusOK, eps)
}

func handleGetEndpoint(c *gin.Context) {
	site, epName := strings.ToUpper(c.Param("site")), c.Param("endpoint")
	ep, ok := iter.GetEndpoint(site, epName)
	if !ok {
		c.JSONP(http.StatusNotFound, gin.H{
			"site":     site,
			"endpoint": epName,
			"message":  "Endpoint not tracking by backend",
		})
		return
	}
	c.JSONP(http.StatusOK, ep)
}

func handlePostEndpoint(c *gin.Context) {
	jsonData, err := io.ReadAll(c.Request.Body)
	if err != nil {
		c.JSONP(http.StatusInternalServerError, gin.H{
			"error":   err.Error(),
			"message": "Unable to read request body",
		})
		return
	}
	var opts RequestBody
	if err = json.Unmarshal(jsonData, &opts); err != nil {
		c.JSONP(http.StatusBadRequest, gin.H{
			"error":   err.Error(),
			"message": "Unable to parse request body",
		})
		return
	}
	site, epName := strings.ToUpper(c.Param("site")), c.Param("endpoint")
	ep, ok := iter.GetEndpoint(site, epName)
	if !ok {
		c.JSONP(http.StatusNotFound, gin.H{
			"site":     site,
			"endpoint": epName,
			"message":  "Endpoint not tracking by backend",
		})
		return
	}
	switch opts.Action {
	case "scan":
		if err = ep.Fetch(iter.GetStorage()); err != nil {
			c.JSONP(http.StatusInternalServerError, gin.H{
				"error":   err.Error(),
				"message": "Unable to fetch endpoint",
			})
			return
		}
		c.JSONP(http.StatusAccepted, ep)
	default:
		c.JSONP(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("invalid action '%s'", opts.Action),
		})
		return
	}
}

func handleSearch(c *gin.Context) {
	// Search By ids
	if ids := c.Query("ids"); ids != "" {
		var eps []endpoint.Endpoint
		iter.IterateOnce(func(ep endpoint.Endpoint) (error, bool) {
			if result := ep.SearchByIDs(strings.Split(ids, ",")); result != nil {
				eps = append(eps, result)
			}
			return nil, true
		})

		c.JSONP(http.StatusOK, eps)
		return
	}

	params, err := url.ParseQuery(c.Request.URL.RawQuery)
	if err != nil {
		c.JSONP(http.StatusInternalServerError, gin.H{
			"message": "Unable to parse URI params",
			"error":   err.Error(),
		})
		return
	}
	opts := make(map[string]any)
	for key, value := range params {
		switch key {
		case "name":
			re, err := regexp.Compile(value[0])
			if err != nil {
				c.JSONP(http.StatusBadRequest, gin.H{
					"error":   err.Error(),
					"message": fmt.Sprintf("Unable to compile regexp %s", value[0]),
				})
				return
			}
			opts[key] = re
		default:
			if len(value) == 1 {
				if b, err := strconv.ParseBool(value[0]); err != nil {
					opts[key] = value[0]
				} else {
					opts[key] = b
				}
			} else {
				opts[key] = value
			}
		}
	}

	eps := iter.Search(opts)
	c.JSONP(http.StatusOK, eps)
}

//func contains(value string, arr []string) bool {
//	for _, elem := range arr {
//		if value == elem {
//			return true
//		}
//	}
//	return false
//}
