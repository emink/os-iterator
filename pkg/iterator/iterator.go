package iterator

import (
	"fmt"
	"gitlab.com/emink/os-iterator/pkg/config"
	"gitlab.com/emink/os-iterator/pkg/iterator/cinder"
	"gitlab.com/emink/os-iterator/pkg/iterator/compute"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/glance"
	"gitlab.com/emink/os-iterator/pkg/iterator/ironic"
	"gitlab.com/emink/os-iterator/pkg/iterator/neutron"
	"gitlab.com/emink/os-iterator/pkg/iterator/octavia"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/utils"
)

type Iterator interface {
	SetIntervals(attempts int, interval int) Iterator
	Auth() Iterator
	Fetch()
	Load() Iterator
	Search(filters map[string]any) []endpoint.Endpoint
	Iterate(fn func(endpoint.Endpoint) (error, bool))
	IterateOnce(fn func(endpoint.Endpoint) (error, bool))
	GetSite(site string) ([]endpoint.Endpoint, bool)
	GetEndpoint(site string, epName string) (endpoint.Endpoint, bool)
	GetStorage() store.Storage
	Rescan(site string, epName string) (endpoint.Endpoint, error)
	//MarshalJSON() ([]byte, error)
}

type Resources struct {
	Endpoints   []endpoint.Endpoint `json:"endpoints"`
	Storage     store.Storage       `json:"-"`
	Attempts    int                 `json:"-"`
	Parallelism int                 `json:"-"`
	Interval    int                 `json:"-"`
}

func New(cfg *config.Config, s store.Storage) Iterator {
	var eps []endpoint.Endpoint
	for _, site := range cfg.Sites {
		for _, epName := range cfg.Endpoints {
			var ep endpoint.Endpoint
			switch epName {
			case "baremetal":
				ep = &ironic.BareMetal{Site: site, Type: epName}
			case "compute":
				ep = &compute.Compute{Site: site, Type: epName}
			case "network":
				ep = &neutron.Network{Site: site, Type: epName}
			case "port":
				ep = &neutron.Port{Site: site, Type: epName}
			case "volume":
				ep = &cinder.Volume{Site: site, Type: epName}
			case "image":
				ep = &glance.Image{Site: site, Type: epName}
			case "loadbalancer":
				ep = &octavia.LoadBalancer{Site: site, Type: epName}
			default:
				continue
			}
			eps = append(eps, ep)
		}
	}
	return &Resources{
		Endpoints: eps,
		Storage:   s,
		Attempts:  cfg.Attempts, Parallelism: cfg.Parallelism, Interval: cfg.Interval,
	}
}

func (r *Resources) SetIntervals(attempts int, interval int) Iterator {
	r.Attempts, r.Interval = attempts, interval
	log.WithFields(log.Fields{"attempts": attempts, "interval": interval}).Info("Set intervals")
	return r
}

func (r *Resources) Auth() Iterator {
	r.iterate(r.Attempts, r.Parallelism, 0, func(ep endpoint.Endpoint) (err error, ok bool) {
		if err = ep.Auth(); err != nil {
			if err.Error() == utils.ErrNoSuitableEndpoint {
				log.WithFields(log.Fields{
					"cloud":    ep.GetSite(),
					"endpoint": ep.GetType(),
					"error":    err,
				}).Debug("Skipped")
				ok = true
			}
			return
		}
		ok = true
		return
	})
	return r
}

func (r *Resources) Fetch() {
	r.Iterate(func(ep endpoint.Endpoint) (error, bool) {
		if err := ep.Fetch(r.Storage); err != nil {
			return err, false
		}
		return nil, true
	})
}

func (r *Resources) Load() Iterator {
	r.IterateOnce(func(ep endpoint.Endpoint) (err error, ok bool) {
		if err = ep.Load(r.Storage); err != nil && err.Error() != "redis: nil" {
			return
		}
		ok = true
		return
	})
	return r
}

func (r *Resources) Iterate(fn func(ep endpoint.Endpoint) (err error, ok bool)) {
	r.iterate(r.Attempts, r.Parallelism, r.Interval, fn)
}

func (r *Resources) IterateOnce(fn func(ep endpoint.Endpoint) (err error, ok bool)) {
	r.iterate(1, r.Parallelism, 0, fn)
}

func (r *Resources) iterate(attempts, parallelism int, interval int, fn func(ep endpoint.Endpoint) (err error, ok bool)) {
	for {
		start := time.Now()
		wg := sync.WaitGroup{}
		ch := make(chan struct{}, parallelism)
		for _, ep := range r.Endpoints {
			ch <- struct{}{}
			wg.Add(1)
			go func(ep endpoint.Endpoint) {
				defer func() {
					wg.Done()
					<-ch
				}()
				site, epName := ep.GetSite(), ep.GetType()
				for i := 0; i < attempts; i++ {
					elapsed := time.Now()
					if err, ok := fn(ep); !ok {
						log.WithFields(log.Fields{
							"cloud":    site,
							"endpoint": epName,
							"attempt":  i + 1,
							"err":      err,
						}).Error("Error occurred inside fn()")
						if i == attempts-1 {
							//ep.SetError(err)
							log.WithFields(log.Fields{
								"cloud":    site,
								"endpoint": epName,
								"attempts": attempts,
							}).Error("Max attempts exceeded")
						}
						continue
					}
					log.WithFields(log.Fields{
						"cloud":    site,
						"endpoint": epName,
						"elapsed":  time.Since(elapsed),
					}).Debug("Elapsed")
					break
				}
			}(ep)
		}

		wg.Wait()
		close(ch)
		if interval == 0 {
			break
		}
		utils.Sleep(start, interval)
	}
}

func (r *Resources) Search(filters map[string]any) []endpoint.Endpoint {
	var eps []endpoint.Endpoint
	r.IterateOnce(func(ep endpoint.Endpoint) (error, bool) {
		ids, selected := ep.Search(filters)
		if len(ids) > 0 {
			eps = append(eps, selected)
			site, epName := ep.GetSite(), ep.GetType()
			log.WithFields(log.Fields{
				"cloud":    site,
				"endpoint": epName,
				"ids":      strings.Join(ids, ","),
				"count":    len(ids),
			}).Info("Matched r")
		}
		return nil, true
	})
	return eps
}

func (r *Resources) GetSite(site string) ([]endpoint.Endpoint, bool) {
	var eps []endpoint.Endpoint
	var count int
	for _, ep := range r.Endpoints {
		if site == ep.GetSite() {
			eps = append(eps, ep)
			count++
		}
	}
	return eps, count != 0
}

func (r *Resources) GetEndpoint(site string, epName string) (endpoint.Endpoint, bool) {
	for _, ep := range r.Endpoints {
		if site == ep.GetSite() && epName == ep.GetType() {
			return ep, true

		}
	}
	return nil, false
}

func (r *Resources) GetStorage() store.Storage {
	return r.Storage
}

func (r *Resources) Rescan(site string, epName string) (endpoint.Endpoint, error) {
	for _, ep := range r.Endpoints {
		if site == ep.GetSite() && epName == ep.GetType() {
			err := ep.Fetch(r.Storage)
			if err != nil {
				return nil, err
			}
			return ep, nil
		}
	}
	return nil, fmt.Errorf("endpoint not found")
}

//func (r *Resources) MarshalJSON() ([]byte, error) {
//	return json.Marshal(map[string]any{"total": len(r.Endpoints), "endpoints": r.Endpoints})
//}
