package endpoint

import (
	"github.com/gophercloud/gophercloud"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
)

type Endpoint interface {
	Auth() error
	Fetch(s store.Storage) error
	Search(filters map[string]any) (ids []string, e Endpoint)
	Load(s store.Storage) error
	GetServiceClient() *gophercloud.ServiceClient
	GetSite() string
	GetType() string
	MarshalJSON() ([]byte, error)
	UnmarshalJSON(data []byte) error
	SearchByIDs(ids []string) Endpoint
}
