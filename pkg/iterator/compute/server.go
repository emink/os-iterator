package compute

import (
	"encoding/json"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"gitlab.com/emink/os-iterator/pkg/utils"
	"sync"
)

const openstackEndpoint = "compute"

type Compute struct {
	*gophercloud.ServiceClient
	sync.Mutex
	Site    string           `json:"site"`
	Type    string           `json:"type"`
	Servers []servers.Server `json:"servers"`
}

func (ep *Compute) Auth() (err error) {
	ep.ServiceClient, err = utils.NewServiceClient(ep.Site, openstackEndpoint)
	if err != nil {
		return err
	}
	return
}

func (ep *Compute) GetServiceClient() *gophercloud.ServiceClient {
	return ep.ServiceClient
}

func (ep *Compute) GetSite() string {
	return ep.Site
}

func (ep *Compute) GetType() string {
	return ep.Type
}

func (ep *Compute) Fetch(s store.Storage) (err error) {
	if ep.ServiceClient != nil {
		if err = ep.listServers(); err != nil {
			return
		}
		err = ep.save(s)
	}
	return
}

func (ep *Compute) Search(filters map[string]interface{}) ([]string, endpoint.Endpoint) {
	//pub_net := []map[string]interface{}{
	//	{
	//		"OS-EXT-IPS:type": "fixed",
	//		"addr":            "185.249.135.24",
	//		"version":         4,
	//	},
	//}
	//filters["addresses"] = map[string]interface{}{
	//	"pub_net": pub_net,
	//}
	var ids []string
	result := &Compute{Site: ep.Site, Type: ep.Type}
	for _, r := range ep.Servers {
		if utils.CompareResource(r, filters) {
			ids = append(ids, r.ID)
			result.Servers = append(result.Servers, r)
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"id":       r.ID,
				"name":     r.Name,
				"status":   r.Status,
			}).Info("Match resource")
		}
	}
	return ids, result
}

func (ep *Compute) SearchByIDs(ids []string) endpoint.Endpoint {
	result := &Compute{Site: ep.Site, Type: ep.Type}
	for _, res := range ep.Servers {
		for _, id := range ids {
			if id == res.ID {
				result.Servers = append(result.Servers, res)
			}
		}
	}
	if len(result.Servers) > 0 {
		return result
	}
	return nil
}

func (ep *Compute) Load(s store.Storage) (err error) {
	return ep.load(s)
}

func (ep *Compute) save(s store.Storage) error {
	if len(ep.Servers) > 0 {
		if err := s.Write(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil {
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"error":    err,
			}).Error("Unable to save data")
			return err
		}
		return nil
	}
	return nil
}

func (ep *Compute) load(s store.Storage) error {
	if err := s.Read(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil && err.Error() != "redis: nil" {
		log.WithFields(log.Fields{
			"cloud":    ep.Site,
			"endpoint": ep.Type,
			"error":    err,
		}).Error("Unable to load data")
		return err
	}
	return nil
}

func (ep *Compute) listServers() error {
	ep.Lock()
	defer ep.Unlock()
	p, err := servers.List(ep.ServiceClient, &servers.ListOpts{AllTenants: true}).AllPages()
	if err != nil {
		return err
	}
	ep.Servers, err = servers.ExtractServers(p)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"cloud":    ep.Site,
		"endpoint": ep.Type,
		"count":    len(ep.Servers),
	}).Info("Processed resources")
	return nil
}

func (ep *Compute) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{"site": ep.Site, "type": ep.Type, "count": len(ep.Servers), "resources": ep.Servers})
}

func (ep *Compute) UnmarshalJSON(data []byte) error {
	s := &struct {
		Resources []servers.Server `json:"resources"`
	}{}
	if err := json.Unmarshal(data, s); err != nil {
		return err
	}
	if len(s.Resources) > 0 {
		ep.Servers = s.Resources
	}
	return nil
}
