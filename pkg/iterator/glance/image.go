package glance

import (
	"encoding/json"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/images"
	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"gitlab.com/emink/os-iterator/pkg/utils"
	"sync"
)

const openstackEndpoint = "image"

type Image struct {
	*gophercloud.ServiceClient
	sync.Mutex
	Site   string         `json:"site"`
	Type   string         `json:"type"`
	Images []images.Image `json:"images"`
}

func (ep *Image) Auth() (err error) {
	ep.ServiceClient, err = utils.NewServiceClient(ep.Site, openstackEndpoint)
	return
}

func (ep *Image) GetServiceClient() *gophercloud.ServiceClient {
	return ep.ServiceClient
}

func (ep *Image) GetSite() string {
	return ep.Site
}

func (ep *Image) GetType() string {
	return ep.Type
}

func (ep *Image) Fetch(s store.Storage) (err error) {
	if ep.ServiceClient != nil {
		if err = ep.listImages(); err != nil {
			return
		}
		err = ep.save(s)
	}
	return
}

func (ep *Image) Search(filters map[string]interface{}) ([]string, endpoint.Endpoint) {
	var ids []string
	result := &Image{Site: ep.Site, Type: ep.Type}
	for _, r := range ep.Images {
		if utils.CompareResource(r, filters) {
			ids = append(ids, r.ID)
			result.Images = append(result.Images, r)
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"id":       r.ID,
				"name":     r.Name,
				"status":   r.Status,
			}).Info("Match resource")
		}
	}
	return ids, result
}

func (ep *Image) SearchByIDs(ids []string) endpoint.Endpoint {
	result := &Image{Site: ep.Site, Type: ep.Type}
	for _, res := range ep.Images {
		for _, id := range ids {
			if id == res.ID {
				result.Images = append(result.Images, res)
			}
		}
	}
	if len(result.Images) > 0 {
		return result
	}
	return nil
}

func (ep *Image) Load(s store.Storage) error {
	return ep.load(s)
}

func (ep *Image) save(s store.Storage) error {
	if len(ep.Images) > 0 {
		if err := s.Write(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil {
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"error":    err,
			}).Warn("Unable to save data")
			return err
		}
		return nil
	}
	return nil
}

func (ep *Image) load(s store.Storage) error {
	if err := s.Read(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil && err.Error() != "redis: nil" {
		log.WithFields(log.Fields{
			"cloud":    ep.Site,
			"endpoint": ep.Type,
			"error":    err,
		}).Error("Unable to load data")
		return err
	}
	return nil
}

func (ep *Image) listImages() error {
	ep.Lock()
	defer ep.Unlock()
	p, err := images.List(ep.ServiceClient, &images.ListOpts{}).AllPages()
	if err != nil {
		return err
	}
	ep.Images, err = images.ExtractImages(p)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"cloud":    ep.Site,
		"endpoint": ep.Type,
		"count":    len(ep.Images),
	}).Info("Processed resources")
	return nil
}

func (ep *Image) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{"site": ep.Site, "type": ep.Type, "count": len(ep.Images), "resources": ep.Images})
}

func (ep *Image) UnmarshalJSON(data []byte) error {
	s := &struct {
		Resources []images.Image `json:"resources"`
	}{}
	if err := json.Unmarshal(data, s); err != nil {
		return err
	}
	if len(s.Resources) > 0 {
		ep.Images = s.Resources
	}
	return nil
}
