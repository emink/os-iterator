package store

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"time"
)

type Storage interface {
	Write(key string, m json.Marshaler) error
	Read(key string, u json.Unmarshaler) error
}

type RedisDB struct {
	*redis.Client
	ctx context.Context
}

func New(opts map[string]string) Storage {
	if dbType, ok := opts["type"]; ok {
		switch dbType {
		case "redis":
			return &RedisDB{
				Client: redis.NewClient(&redis.Options{
					Addr:     opts["address"],
					Password: opts["password"],
					DB:       0,
				}),
				ctx: context.Background(),
			}
		default:
			return nil
		}
	}
	return nil
}

func (s *RedisDB) Write(key string, m json.Marshaler) error {
	b, err := m.MarshalJSON()
	if err != nil {
		return err
	}
	err = s.Client.Set(s.ctx, key, b, time.Minute*360).Err()
	if err != nil {
		return err
	}
	return nil
}

func (s *RedisDB) Read(key string, u json.Unmarshaler) error {
	value, err := s.Client.Get(s.ctx, key).Result()
	if err != nil {
		return err
	}
	return u.UnmarshalJSON([]byte(value))
}
