package neutron

import (
	"encoding/json"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/ports"
	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"gitlab.com/emink/os-iterator/pkg/utils"
	"sync"
)

type Port struct {
	*gophercloud.ServiceClient
	sync.Mutex
	Site  string       `json:"site"`
	Type  string       `json:"type"`
	Ports []ports.Port `json:"ports"`
}

func (ep *Port) Auth() (err error) {
	ep.ServiceClient, err = utils.NewServiceClient(ep.Site, openstackEndpoint)
	return
}

func (ep *Port) GetServiceClient() *gophercloud.ServiceClient {
	return ep.ServiceClient
}

func (ep *Port) GetSite() string {
	return ep.Site
}

func (ep *Port) GetType() string {
	return ep.Type
}

func (ep *Port) Fetch(s store.Storage) (err error) {
	if ep.ServiceClient != nil {
		if err = ep.listPorts(); err != nil {
			return
		}
		err = ep.save(s)
	}
	return
}

func (ep *Port) Search(filters map[string]interface{}) ([]string, endpoint.Endpoint) {
	var ids []string
	result := &Port{Site: ep.Site, Type: ep.Type}
	for _, r := range ep.Ports {
		if utils.CompareResource(r, filters) {
			ids = append(ids, r.ID)
			result.Ports = append(result.Ports, r)
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"id":       r.ID,
				"name":     r.Name,
				"status":   r.Status,
			}).Info("Match resource")
		}
	}
	return ids, result
}

func (ep *Port) SearchByIDs(ids []string) endpoint.Endpoint {
	result := &Port{Site: ep.Site, Type: ep.Type}
	for _, res := range ep.Ports {
		for _, id := range ids {
			if id == res.ID {
				result.Ports = append(result.Ports, res)
			}
		}
	}
	if len(result.Ports) > 0 {
		return result
	}
	return nil
}

func (ep *Port) Load(s store.Storage) error {
	return ep.load(s)
}

func (ep *Port) save(s store.Storage) error {
	if len(ep.Ports) > 0 {
		if err := s.Write(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil {
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"error":    err,
			}).Warn("Unable to save data")
			return err
		}
		return nil
	}
	return nil
}

func (ep *Port) load(s store.Storage) error {
	if err := s.Read(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil && err.Error() != "redis: nil" {
		log.WithFields(log.Fields{
			"cloud":    ep.Site,
			"endpoint": ep.Type,
			"error":    err,
		}).Error("Unable to load data")
		return err
	}
	return nil
}

func (ep *Port) listPorts() error {
	ep.Lock()
	defer ep.Unlock()
	p, err := ports.List(ep.ServiceClient, nil).AllPages()
	if err != nil {
		return err
	}
	ep.Ports, err = ports.ExtractPorts(p)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"cloud":    ep.Site,
		"endpoint": ep.Type,
		"count":    len(ep.Ports),
	}).Info("Processed resources")
	return nil
}

func (ep *Port) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{"site": ep.Site, "type": ep.Type, "count": len(ep.Ports), "resources": ep.Ports})
}

func (ep *Port) UnmarshalJSON(data []byte) error {
	s := &struct {
		Resources []ports.Port `json:"resources"`
	}{}
	if err := json.Unmarshal(data, s); err != nil {
		return err
	}
	if len(s.Resources) > 0 {
		ep.Ports = s.Resources
	}
	return nil
}
