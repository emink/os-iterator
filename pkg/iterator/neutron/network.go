package neutron

import (
	"encoding/json"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"gitlab.com/emink/os-iterator/pkg/utils"
	"sync"
)

const openstackEndpoint = "network"

type Network struct {
	*gophercloud.ServiceClient
	sync.Mutex
	Site     string             `json:"site"`
	Type     string             `json:"type"`
	Networks []networks.Network `json:"networks"`
}

func (ep *Network) Auth() (err error) {
	ep.ServiceClient, err = utils.NewServiceClient(ep.Site, openstackEndpoint)
	return
}

func (ep *Network) GetServiceClient() *gophercloud.ServiceClient {
	return ep.ServiceClient
}

func (ep *Network) GetSite() string {
	return ep.Site
}

func (ep *Network) GetType() string {
	return ep.Type
}

func (ep *Network) Fetch(s store.Storage) (err error) {
	if ep.ServiceClient != nil {
		if err = ep.listNetworks(); err != nil {
			return
		}
		err = ep.save(s)
	}
	return
}

func (ep *Network) Search(filters map[string]interface{}) ([]string, endpoint.Endpoint) {
	var ids []string
	result := &Network{Site: ep.Site, Type: ep.Type}
	for _, r := range ep.Networks {
		if utils.CompareResource(r, filters) {
			ids = append(ids, r.ID)
			result.Networks = append(result.Networks, r)
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"id":       r.ID,
				"name":     r.Name,
				"status":   r.Status,
			}).Info("Match resource")
		}
	}
	return ids, result
}

func (ep *Network) SearchByIDs(ids []string) endpoint.Endpoint {
	result := &Network{Site: ep.Site, Type: ep.Type}
	for _, res := range ep.Networks {
		for _, id := range ids {
			if id == res.ID {
				result.Networks = append(result.Networks, res)
			}
		}
	}
	if len(result.Networks) > 0 {
		return result
	}
	return nil
}

func (ep *Network) Load(s store.Storage) error {
	return ep.load(s)
}

func (ep *Network) save(s store.Storage) error {
	if len(ep.Networks) > 0 {
		if err := s.Write(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil {
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"error":    err,
			}).Warn("Unable to save data")
			return err
		}
		return nil
	}
	return nil
}

func (ep *Network) load(s store.Storage) error {
	if err := s.Read(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil && err.Error() != "redis: nil" {
		log.WithFields(log.Fields{
			"cloud":    ep.Site,
			"endpoint": ep.Type,
			"error":    err,
		}).Error("Unable to load data")
		return err
	}
	return nil
}

func (ep *Network) listNetworks() error {
	ep.Lock()
	defer ep.Unlock()
	p, err := networks.List(ep.ServiceClient, nil).AllPages()
	if err != nil {
		return err
	}
	ep.Networks, err = networks.ExtractNetworks(p)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"cloud":    ep.Site,
		"endpoint": ep.Type,
		"count":    len(ep.Networks),
	}).Info("Processed resources")
	return nil
}

func (ep *Network) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{"site": ep.Site, "type": ep.Type, "count": len(ep.Networks), "resources": ep.Networks})
}

func (ep *Network) UnmarshalJSON(data []byte) error {
	s := &struct {
		Resources []networks.Network `json:"resources"`
	}{}
	if err := json.Unmarshal(data, s); err != nil {
		return err
	}
	if len(s.Resources) > 0 {
		ep.Networks = s.Resources
	}
	return nil
}
