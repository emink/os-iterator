package cinder

import (
	"encoding/json"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/blockstorage/v3/volumes"
	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"gitlab.com/emink/os-iterator/pkg/utils"
	"sync"
)

const openstackEndpoint = "volume"

type Volume struct {
	*gophercloud.ServiceClient
	sync.Mutex
	Site    string           `json:"site"`
	Type    string           `json:"type"`
	Volumes []volumes.Volume `json:"volumes"`
}

func (ep *Volume) Auth() (err error) {
	ep.ServiceClient, err = utils.NewServiceClient(ep.Site, openstackEndpoint)
	return
}

func (ep *Volume) GetServiceClient() *gophercloud.ServiceClient {
	return ep.ServiceClient
}

func (ep *Volume) GetSite() string {
	return ep.Site
}

func (ep *Volume) GetType() string {
	return ep.Type
}

func (ep *Volume) Fetch(s store.Storage) (err error) {
	if ep.ServiceClient != nil {
		if err = ep.listVolumes(); err != nil {
			return
		}
		err = ep.save(s)
	}
	return
}

func (ep *Volume) Search(filters map[string]interface{}) ([]string, endpoint.Endpoint) {
	var ids []string
	result := &Volume{Site: ep.Site, Type: ep.Type}
	for _, r := range ep.Volumes {
		if utils.CompareResource(r, filters) {
			ids = append(ids, r.ID)
			result.Volumes = append(result.Volumes, r)
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"id":       r.ID,
				"name":     r.Name,
				"status":   r.Status,
				"size":     r.Size,
			}).Info("Match resource")
		}
	}
	return ids, result
}

func (ep *Volume) SearchByIDs(ids []string) endpoint.Endpoint {
	result := &Volume{Site: ep.Site, Type: ep.Type}
	for _, res := range ep.Volumes {
		for _, id := range ids {
			if id == res.ID {
				result.Volumes = append(result.Volumes, res)
			}
		}
	}
	if len(result.Volumes) > 0 {
		return result
	}
	return nil
}

func (ep *Volume) Load(s store.Storage) error {
	return ep.load(s)
}

func (ep *Volume) save(s store.Storage) error {
	if len(ep.Volumes) > 0 {
		if err := s.Write(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil {
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"error":    err,
			}).Error("Unable to save data")
			return err
		}
		return nil
	}
	return nil
}

func (ep *Volume) load(s store.Storage) error {
	if err := s.Read(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil && err.Error() != "redis: nil" {
		log.WithFields(log.Fields{
			"cloud":    ep.Site,
			"endpoint": ep.Type,
			"error":    err,
		}).Error("Unable to load data")
		return err
	}
	return nil
}

func (ep *Volume) listVolumes() error {
	ep.Lock()
	defer ep.Unlock()
	p, err := volumes.List(ep.ServiceClient, &volumes.ListOpts{AllTenants: true}).AllPages()
	if err != nil {
		return err
	}
	ep.Volumes, err = volumes.ExtractVolumes(p)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"cloud":    ep.Site,
		"endpoint": ep.Type,
		"count":    len(ep.Volumes),
	}).Info("Processed resources")
	return nil
}

func (ep *Volume) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{"site": ep.Site, "type": ep.Type, "count": len(ep.Volumes), "resources": ep.Volumes})
}

func (ep *Volume) UnmarshalJSON(data []byte) error {
	s := &struct {
		Resources []volumes.Volume `json:"resources"`
	}{}
	if err := json.Unmarshal(data, s); err != nil {
		return err
	}
	if len(s.Resources) > 0 {
		ep.Volumes = s.Resources
	}
	return nil
}
