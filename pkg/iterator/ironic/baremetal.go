package ironic

import (
	"encoding/json"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/baremetal/v1/nodes"
	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"gitlab.com/emink/os-iterator/pkg/utils"
	"sync"
)

const openstackEndpoint = "baremetal"

type BareMetal struct {
	*gophercloud.ServiceClient
	sync.Mutex
	Site  string       `json:"site"`
	Type  string       `json:"type"`
	Nodes []nodes.Node `json:"nodes"`
}

func (ep *BareMetal) Auth() (err error) {
	ep.ServiceClient, err = utils.NewServiceClient(ep.Site, openstackEndpoint)
	return
}

func (ep *BareMetal) GetServiceClient() *gophercloud.ServiceClient {
	return ep.ServiceClient
}

func (ep *BareMetal) GetSite() string {
	return ep.Site
}

func (ep *BareMetal) GetType() string {
	return ep.Type
}

func (ep *BareMetal) Fetch(s store.Storage) (err error) {
	if ep.ServiceClient != nil {
		if err = ep.listNodesDetail(); err != nil {
			return
		}
		err = ep.save(s)
	}
	return
}

func (ep *BareMetal) Search(filters map[string]interface{}) ([]string, endpoint.Endpoint) {
	var ids []string
	result := &BareMetal{Site: ep.Site, Type: ep.Type}
	for _, r := range ep.Nodes {
		if utils.CompareResource(r, filters) {
			ids = append(ids, r.UUID)
			result.Nodes = append(result.Nodes, r)
			log.WithFields(log.Fields{
				"cloud":           ep.Site,
				"endpoint":        ep.Type,
				"id":              r.UUID,
				"name":            r.Name,
				"provision_state": r.ProvisionState,
				"resource_class":  r.ResourceClass,
				"instance_uuid":   r.InstanceUUID,
				"maintenance":     r.Maintenance,
			}).Info("Match resource")
		}
	}
	return ids, result
}

func (ep *BareMetal) SearchByIDs(ids []string) endpoint.Endpoint {
	result := &BareMetal{Site: ep.Site, Type: ep.Type}
	for _, res := range ep.Nodes {
		for _, id := range ids {
			if id == res.UUID {
				result.Nodes = append(result.Nodes, res)
			}
		}
	}
	if len(result.Nodes) > 0 {
		return result
	}
	return nil
}

func (ep *BareMetal) Load(s store.Storage) error {
	return ep.load(s)
}

func (ep *BareMetal) save(s store.Storage) error {
	if len(ep.Nodes) > 0 {
		if err := s.Write(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil {
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"error":    err,
			}).Warn("Unable to save data")
			return err
		}
		return nil
	}
	return nil
}

func (ep *BareMetal) load(s store.Storage) error {
	if err := s.Read(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil && err.Error() != "redis: nil" {
		log.WithFields(log.Fields{
			"cloud":    ep.Site,
			"endpoint": ep.Type,
			"error":    err,
		}).Error("Unable to load data")
		return err
	}
	return nil
}

func (ep *BareMetal) listNodesDetail() error {
	ep.Lock()
	defer ep.Unlock()
	p, err := nodes.ListDetail(ep.ServiceClient, nil).AllPages()
	if err != nil {
		return err
	}
	ep.Nodes, err = nodes.ExtractNodes(p)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"cloud":    ep.Site,
		"endpoint": ep.Type,
		"count":    len(ep.Nodes),
	}).Info("Processed resources")
	return nil
}

//func (ep *BareMetal) UpdateKernelRamdisk(kernel, ramdisk string) error {
//	opts := nodes.UpdateOpts{}
//	opts = append(opts, nodes.UpdateOperation{
//		Op:    "add",
//		Path:  "/driver_info/deploy_kernel",
//		Value: kernel,
//	}, nodes.UpdateOperation{
//		Op:    "add",
//		Path:  "/driver_info/deploy_ramdisk",
//		Value: ramdisk,
//	})
//	for _, r := range ep.Selected {
//		if !r.Maintenance {
//			bm, err := nodes.Update(ep.ServiceClient, r.UUID, opts).Extract()
//			if err != nil {
//				log.WithFields(log.Fields{"cloud": ep.Site, "endpoint": ep.Type, "error": err}).Warn("Unable to update kernel and ramdisk")
//				return err
//			}
//			log.WithFields(log.Fields{
//				"site":            ep.Site,
//				"endpoint":        ep.Type,
//				"uuid":            r.UUID,
//				"name":            r.Name,
//				"new_kernel":      bm.DriverInfo["deploy_kernel"],
//				"new_ramdisk":     bm.DriverInfo["deploy_ramdisk"],
//				"provision_state": r.ProvisionState,
//			}).Info("Updated kernel and ramdisk")
//		}
//	}
//	return nil
//}

func (ep *BareMetal) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{"site": ep.Site, "type": ep.Type, "count": len(ep.Nodes), "resources": ep.Nodes})
}

func (ep *BareMetal) UnmarshalJSON(data []byte) error {
	s := &struct {
		Resources []nodes.Node `json:"resources"`
	}{}
	if err := json.Unmarshal(data, s); err != nil {
		return err
	}
	if len(s.Resources) > 0 {
		ep.Nodes = s.Resources
	}
	return nil
}
