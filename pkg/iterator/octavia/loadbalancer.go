package octavia

import (
	"encoding/json"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/loadbalancer/v2/loadbalancers"
	log "github.com/sirupsen/logrus"
	"gitlab.com/emink/os-iterator/pkg/iterator/endpoint"
	"gitlab.com/emink/os-iterator/pkg/iterator/store"
	"gitlab.com/emink/os-iterator/pkg/utils"
	"sync"
)

const openstackEndpoint = "load-balancer"

type LoadBalancer struct {
	*gophercloud.ServiceClient
	sync.Mutex
	Site          string                       `json:"site"`
	Type          string                       `json:"type"`
	LoadBalancers []loadbalancers.LoadBalancer `json:"load_balancers"`
}

func (ep *LoadBalancer) Auth() (err error) {
	ep.ServiceClient, err = utils.NewServiceClient(ep.Site, openstackEndpoint)
	return
}

func (ep *LoadBalancer) GetServiceClient() *gophercloud.ServiceClient {
	return ep.ServiceClient
}

func (ep *LoadBalancer) GetSite() string {
	return ep.Site
}

func (ep *LoadBalancer) GetType() string {
	return ep.Type
}

func (ep *LoadBalancer) Fetch(s store.Storage) (err error) {
	if ep.ServiceClient != nil {
		if err = ep.listLoadBalancers(); err != nil {
			return
		}
		err = ep.save(s)
	}
	return
}

func (ep *LoadBalancer) Search(filters map[string]interface{}) ([]string, endpoint.Endpoint) {
	var ids []string
	result := &LoadBalancer{Site: ep.Site, Type: ep.Type}
	for _, r := range ep.LoadBalancers {
		if utils.CompareResource(r, filters) {
			ids = append(ids, r.ID)
			result.LoadBalancers = append(result.LoadBalancers, r)
			log.WithFields(log.Fields{
				"cloud":               ep.Site,
				"endpoint":            ep.Type,
				"id":                  r.ID,
				"name":                r.Name,
				"provisioning_status": r.ProvisioningStatus,
				"operating_status":    r.OperatingStatus,
			}).Info("Match resource")
		}
	}
	return ids, result
}

func (ep *LoadBalancer) SearchByIDs(ids []string) endpoint.Endpoint {
	result := &LoadBalancer{Site: ep.Site, Type: ep.Type}
	for _, res := range ep.LoadBalancers {
		for _, id := range ids {
			if id == res.ID {
				result.LoadBalancers = append(result.LoadBalancers, res)
			}
		}
	}
	if len(result.LoadBalancers) > 0 {
		return result
	}
	return nil
}

func (ep *LoadBalancer) Load(s store.Storage) (err error) {
	return ep.load(s)
}

func (ep *LoadBalancer) save(s store.Storage) error {
	if len(ep.LoadBalancers) > 0 {
		if err := s.Write(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil {
			log.WithFields(log.Fields{
				"cloud":    ep.Site,
				"endpoint": ep.Type,
				"error":    err,
			}).Warn("Unable to save data")
			return err
		}
		return nil
	}
	return nil
}

func (ep *LoadBalancer) load(s store.Storage) error {
	if err := s.Read(fmt.Sprintf("%s:%s", ep.Site, ep.Type), ep); err != nil && err.Error() != "redis: nil" {
		log.WithFields(log.Fields{
			"cloud":    ep.Site,
			"endpoint": ep.Type,
			"error":    err,
		}).Error("Unable to load data")
		return err
	}
	return nil
}

func (ep *LoadBalancer) listLoadBalancers() error {
	ep.Lock()
	defer ep.Unlock()
	p, err := loadbalancers.List(ep.ServiceClient, nil).AllPages()
	if err != nil {
		return err
	}
	ep.LoadBalancers, err = loadbalancers.ExtractLoadBalancers(p)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"cloud":    ep.Site,
		"endpoint": ep.Type,
		"count":    len(ep.LoadBalancers),
	}).Info("Processed resources")
	return nil
}

func (ep *LoadBalancer) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{"site": ep.Site, "type": ep.Type, "count": len(ep.LoadBalancers), "resources": ep.LoadBalancers})
}

func (ep *LoadBalancer) UnmarshalJSON(data []byte) error {
	s := &struct {
		Resources []loadbalancers.LoadBalancer `json:"resources"`
	}{}
	if err := json.Unmarshal(data, s); err != nil {
		return err
	}
	if len(s.Resources) > 0 {
		ep.LoadBalancers = s.Resources
	}
	return nil
}
