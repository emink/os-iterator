package config

import (
	"github.com/gophercloud/utils/openstack/clientconfig"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Sites       []string `json:"sites"`
	Endpoints   []string `json:"endpoints"`
	Attempts    int      `json:"attempts"`
	Interval    int      `json:"interval"`
	Parallelism int      `json:"parallelism"`
	Debug       bool     `json:"debug"`
	ConfigPath  string   `json:"config_path"`
}

type CloudsYAML struct {
	Clouds map[string]clientconfig.Cloud `yaml:"clouds"`
}

func ReadCloudsYAML(filename string) (*CloudsYAML, error) {
	_, b, err := clientconfig.FindAndReadYAML(filename)
	if err != nil {
		return nil, err
	}
	cfg := &CloudsYAML{}
	if err = yaml.Unmarshal(b, cfg); err != nil {
		return nil, err
	}
	return cfg, err
}
